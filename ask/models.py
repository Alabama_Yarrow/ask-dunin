from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Profile(models.Model):
	user = models.OneToOneField(User)
	rating = models.IntegerField(default=0)
	avatar_url = models.CharField(max_length=60)
	avatar = models.ImageField(upload_to='', default='nopic.jpg')

class Question(models.Model):
	author = models.ForeignKey(Profile)
	title = models.CharField(max_length=60)
	text = models.TextField()
	rating = models.IntegerField(default=0)
	date_added = models.DateTimeField(auto_now_add=True)

class Answer(models.Model):
	author = models.ForeignKey(Profile)
	question = models.ForeignKey(Question)
	text = models.TextField()
	date_added = models.DateTimeField(auto_now_add=True)
	rating = models.IntegerField(default=0)
	is_right = models.BooleanField(default=False)

class Tag(models.Model):
	tag_word = models.CharField(max_length=60)
	questions = models.ManyToManyField(Question) 

class QuestionVote(models.Model):
	vote = models.IntegerField(default=0)
	user = models.ForeignKey(User)
	question = models.ForeignKey(Question)

class AnswerVote(models.Model):
	vote = models.IntegerField(default=0)
	user = models.ForeignKey(User)
	answer = models.ForeignKey(Answer)

