def application(environ, start_response):
	status = '200 OK'	
	output = 'Hello World!\n'

	requestmethod = environ.get('REQUEST_METHOD')
	output += requestmethod
	output += '\n'

	if requestmethod == "GET":
		querystring = environ.get('QUERY_STRING')
		substrs = querystring.split("&")
		for substr in substrs:
			output += "%s\n" % ( substr )
		
	if requestmethod == "POST":
		length = int(environ.get('CONTENT_LENGTH', 0))
		if length !=0:
			request_body = environ['wsgi.input'].read(length)
			substrs = request_body.split("&")
			for substr in substrs:
				output += "%s\n" % ( substr )

	response_headers = [ ('Content-type', 'text/plain'), ('Context-Length', str(len(output)))]
	start_response(status, response_headers)	
	
	return [output]
