# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ask', '0006_auto_20150110_0926'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='avatar',
            field=models.ImageField(default=b'static/pic1.jpeg', upload_to=b'uploads'),
            preserve_default=True,
        ),
    ]
