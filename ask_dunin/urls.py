from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'ask_dunin.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
	url(r'^hello/$', 'requests.views.hello', name='hello'),
	url(r'^$', 'ask.views.index'),
	url(r'^registration/signup$', 'ask.views.register'),
	url(r'^profile$', 'ask.views.profile'),
	url(r'^login$', 'django.contrib.auth.views.login', {'redirect_field_name': '/question'}),
	url(r'^logout$', 'django.contrib.auth.views.logout'),
	url(r'^question$', 'ask.views.question'),
	url(r'^tag$', 'ask.views.tag'),
	url(r'^like$', 'ask.views.like'),
	url(r'^rightanswer$', 'ask.views.rightanswer'),
	url(r'^ask_question$', 'ask.views.ask_question'),

)

if settings.DEBUG:
	urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)