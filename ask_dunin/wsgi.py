"""
WSGI config for ask_dunin project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/howto/deployment/wsgi/
"""

import os,sys

#sys.path.append('/var/www/ask_dunin')
#sys.path.append('/var/www/venv/lib/python2.7/sites-packages')


os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ask_dunin.settings")

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
