from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.template import RequestContext, loader, Context
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login

from ask.models import *
from ask.forms import *

import json
import os


def index(request):
	sort = request.GET.get('sort')
	if sort == 'hot':
		question_list = Question.objects.order_by('-rating').all()
	else:
		question_list = Question.objects.order_by('-date_added').all()
	
	paginator = Paginator(question_list, 20)
	page = request.GET.get('page')
	try:
		question_page = paginator.page(page)
		min_page = int(page) - 3
		max_page = int(page) + 3
	except PageNotAnInteger:
		question_page = paginator.page(1)
		min_page = 0
		max_page = 4
	except EmptyPage:
		question_page = paginator.page(paginator.num_pages)
		min_page = paginator.num_pages - 3 
		max_page = paginator.num_pages + 1
	return render(request, 'index.html', {'question_page': question_page, \
		'last_page_number': paginator.num_pages, 'sort': sort,  'min_page': min_page, 'max_page': max_page})

def tag(request):
	tag = request.GET.get('tag')
	question_list = Question.objects.filter(tag__tag_word=tag).order_by('-date_added')	
	paginator = Paginator(question_list, 20)
	page = request.GET.get('page')
	try:
		question_page = paginator.page(page)
		min_page = int(page) - 3
		max_page = int(page) + 3
	except PageNotAnInteger:
		question_page = paginator.page(1)
		min_page = 0
		max_page = 4
	except EmptyPage:
		question_page = paginator.page(paginator.num_pages)
		min_page = paginator.num_pages - 3 
		max_page = paginator.num_pages + 1
	return render(request, 'index.html', {'question_page': question_page, \
		'last_page_number': paginator.num_pages, 'tag': tag, 'min_page': min_page, 'max_page': max_page})


def question(request):
	current_question = request.GET.get('id')
	q = Question.objects.get(id = current_question)
	answer_list = Answer.objects.filter(question_id = current_question)
	paginator = Paginator(answer_list, 20)
	page = request.GET.get('page')
	min_page = paginator.num_pages - 3 
	max_page = paginator.num_pages + 3
	try:
		answer_page = paginator.page(page)
		min_page = int(page) - 3
		max_page = int(page) + 3
	except PageNotAnInteger:
		answer_page = paginator.page(paginator.num_pages)
	except EmptyPage:
		answer_page = paginator.page(paginator.num_pages)
	if request.method == 'POST':
		form = AnswerForm(request.POST, user = request.user, question = q)
		if form.is_valid():
			obj=form.save()			
			return HttpResponseRedirect(request.get_full_path())			
	else:
		form = AnswerForm()
	
	return render(request, 'question.html', {'Question': q, \
		'answer_page': answer_page, 'last_page_number': paginator.num_pages, \
		'form': form, 'min_page': min_page, 'max_page': max_page })

@login_required(login_url='/login')
def profile(request):
	user_obj = User.objects.get(id = request.user.id)
	profile_obj = Profile.objects.get(user = user_obj)
	if request.method == 'POST':
		profile_form = SettingsProfileForm(request.POST, request.FILES, instance = profile_obj)
		user_form = SettingsUserForm(request.POST, instance = user_obj)
		if user_form.is_valid() and profile_form.is_valid():
			user_form.save()
			profile_form.save()
			return HttpResponseRedirect(request.get_full_path())			
	profile_form = SettingsProfileForm(instance = profile_obj)
	user_form = SettingsUserForm(instance = user_obj)
	return render(request, 'profile.html', {'profile_form': profile_form, 'user_form': user_form})


def register(request):
	next = '/'
	if request.method == 'POST':
		form = RegistrationForm(request.POST)
		profile_form = RegistrationProfileForm(request.POST, request.FILES)
		if form.is_valid() and profile_form.is_valid():
			user_obj=form.save()			
			profile_obj=profile_form.save(user = user_obj)			
			user_obj=authenticate(username=user_obj.username, password=request.POST['password1'])
			login(request, user_obj)
			return HttpResponseRedirect(next)
	else:
		form = RegistrationForm()
		profile_form = RegistrationProfileForm()
	return render(request, 'registration/signup.html', {'form': form, 'profile_form': profile_form})

def ask_question(request):
	if request.method == 'POST':
		form = QuestionForm(request.POST, user = request.user)
		if form.is_valid():
			question = form.save()
			for tag in form.cleaned_data['tags']:
				try:
					tag = Tag.objects.get(tag_word=tag)
				except Tag.DoesNotExist:
					tag = Tag(tag_word = tag)
					tag.save()
				question.tag_set.add(tag)
			question.save()
			next='/question?id=' + str(question.id)
			return HttpResponseRedirect(next)
	else:
		form = QuestionForm()
	return render(request, 'ask_question.html', {'form': form})

def like(request):
	status = True
	message = "Success"
	pk = request.POST.get('id')
	vote = request.POST.get('vote')
	liketype = request.POST.get('liketype')
	user = User.objects.get(pk = request.user.id)	
	if liketype == 'q':
		rated_obj = Question.objects.get(pk = pk)	
		try:
			user_vote = QuestionVote.objects.get(user = user, question = rated_obj)
		except:
			user_vote = QuestionVote.objects.create(user = user, question = rated_obj)
	else:
		rated_obj = Answer.objects.get(pk = pk)	
		try:
			user_vote = AnswerVote.objects.get(user = user, answer = rated_obj)
		except:
			user_vote = AnswerVote.objects.create(user = user, answer = rated_obj)

	if not request.user.is_authenticated():				
		status = False
		message = "Please log in to vote"
	else:		
		if vote == '+':			
			if user_vote.vote == -1 or user_vote.vote == 0:
				rated_obj.rating += 1
				rated_obj.author.rating += 1
				profile = rated_obj.author
				profile.save()
				user_vote.vote += 1				
				user_vote.save()
			else:
				status = False
				message = "Already liked"
		elif vote == '-':
			if user_vote.vote == +1 or user_vote.vote == 0:
				rated_obj.rating -= 1
				rated_obj.author.rating -= 1				
				profile = rated_obj.author
				profile.save()
				user_vote.vote -= 1				
				user_vote.save()
			else:
				status = False
				message = "Already disliked"
		rated_obj.save()		
	new_rating = rated_obj.rating 
	content = json.dumps({'status': status, 'message': message, 'new_rating': new_rating})

	return HttpResponse(content, content_type='application/json')

def rightanswer(request):
	pk = request.POST.get('id')
	status = True
	message = "hello"
	answer = Answer.objects.get(pk = pk)	
	if answer.is_right:
		new_value = False
		message = ""		
	else:
		new_value = True
		message = "Correct answer"		
	answer.is_right = new_value
	answer.save()

	content = json.dumps({'status': status, 'message': message, 'new_value': new_value})

	return HttpResponse(content, content_type='application/json')








