# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ask', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('tag_word', models.CharField(max_length=60)),
                ('questions', models.ManyToManyField(to='ask.Question')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
