# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ask', '0005_auto_20150110_0822'),
    ]

    operations = [
        migrations.RenameField(
            model_name='answervote',
            old_name='question',
            new_name='answer',
        ),
    ]
