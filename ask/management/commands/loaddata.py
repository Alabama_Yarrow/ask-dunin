from django.core.management.base import BaseCommand
from ask.models import Profile, Question, Answer, Tag
from django.contrib.auth.models import User
from optparse import make_option

from faker.frandom import random
from faker.lorem import sentence,sentences,words
from mixer.fakers import get_username, get_email
from pprint import pformat

from django.db.models import Min, Max

from time import strftime

import csv


class Command(BaseCommand):
	option_list = BaseCommand.option_list + (
		make_option('--users', 
			action='store',
			dest='users',
			default=0	
			),	
		make_option('--questions', 
			action='store',
			dest='questions',
			default=0	
			),	
		make_option('--answers', 
			action='store',
			dest='answers',
			default=0	
			),	
		make_option('--tags', 
			action='store',
			dest='tags',
			default=0	
			),	
		make_option('--qtags', 
			action='store_true',
			dest='qtags',
			default=0	
			),
	)

	#via CSV files:
	#load data local infile '/.../file.csv' 
	#into table ask_table 
	#fields terminated by ',' 
	#(col_1, ... , col_n);

	def handle(self, *args, **options):		
	#users
		names = {}
		while (len( names.keys() ) < int(options['users'])):
			names[get_username(length=30)]=1 #hashtable
		last_id = Profile.objects.all().aggregate(Max('id'))['id__max']
		with open('users.csv', 'wb') as user_csv, open('profiles.csv', 'wb') as prof_csv:
			user_writer = csv.writer(user_csv, delimiter = ',')
			prof_writer = csv.writer(prof_csv, delimiter = ',')			
			for name in names.keys():
				last_id += 1
				#id + username + email + date_joined
				user_writer.writerow([last_id] + [name] + [get_email()] + [strftime("%Y-%m-%d %H:%M:%S")])
				#id + user_id + rating + avatar
				prof_writer.writerow([last_id] + [last_id] + [random.randint(0,20)] + 'nopic.jpg')
		
	#questions
		p_min = Profile.objects.all().aggregate(Min('id'))['id__min']
		p_max = Profile.objects.all().aggregate(Max('id'))['id__max']
		with open('questions.csv', 'wb') as ques_csv:
			ques_writer = csv.writer(ques_csv, delimiter = ',')
			for i in range(0, int(options['questions'])):				
				# author_id + rating + title + text + date_added
				ques_writer.writerow( [random.randint(p_min, p_max)] + [random.randint(0,200)] + \
					[(sentence())[0:59]] + [sentences(3)] + [strftime("%Y-%m-%d %H:%M:%S")])	

	#answers	
		q_min = Question.objects.all().aggregate(Min('id'))['id__min']
		q_max = Question.objects.all().aggregate(Max('id'))['id__max']
		with open('answers.csv', 'wb') as answ_csv:
			answ_writer = csv.writer(answ_csv, delimiter = ',')
			for i in range(0, int(options['answers'])):
				#author_id + question_id + text + date_added
				answ_writer.writerow([random.randint(p_min, p_max)] + \
					[random.randint(q_min, q_max)] + [sentences(2)] + [strftime("%Y-%m-%d %H:%M:%S")])

	#tags
		for i in range(0, int(options['tags'])):
			t = Tag.objects.create(tag_word = (words(1)[0]))

	#set tags for questions
		if (int(options['qtags'])):
			t_min = Tag.objects.all().aggregate(Min('id'))['id__min']
			t_max = Tag.objects.all().aggregate(Max('id'))['id__max']
			with open('questiontags.csv', 'wb') as qtags_csv:				
				qtags_writer = csv.writer(qtags_csv, delimiter = ',')
				for question_id in range(q_min, q_max):					
					tags = random.sample(range(t_min, t_max),3)
					for i in 0,1,2:
						tag_id = tags[i]
						qtags_writer.writerow([question_id] + [tag_id])










